Name:           bloom-shutdown-applet
Version:        0.1.0
Release:        1%{?dist}
Summary:        Bloom Network Manager

Group:          User Interface/Desktops
License:        GPLv2+
URL:            http://gitorious.alpha.agorabox.org/bloom/bloom-shutdown-applet
Source0:        http://gitorious.alpha.agorabox.org/bloom/bloom-shutdown-applet/%{name}-%{version}.tar.bz2
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

%description
Bloom Shutdown Applet

%package devel
Summary: Development package for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
Files for development with %{name}.

%prep
%setup -q

# run autogen.sh until we have a proper release, but don't run configure twice.
#NOCONFIGURE=true ./autogen.sh

./autogen.sh

%build
%configure
make %{?_smp_mflags} j=10

%install
rm -rf %{buildroot}
make install DESTDIR=%{buildroot}

%clean
rm -rf %{buildroot}

%files
%{_bindir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}
%{_datadir}/locale/es/LC_MESSAGES/%{name}.mo
%{_datadir}/locale/fr/LC_MESSAGES/%{name}.mo
%{_datadir}/locale/pt/LC_MESSAGES/%{name}.mo

%changelog
* Mon Jul 27 2010 Antonin Fouques  <antonin.fouques@agorabox.org> - 0.1.0-1
- Initial release + 'working release' : cleaning and coding style + some debug + some change to make it works
