//      bloom-shutdown-applet.cpp
//
//      Copyright Agorabox 2010 <antonin.fouques@agorabox.org>
//
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.

#include <QObject>

#include "bloom-shutdown-applet.h"

BloomShutdownApplet::BloomShutdownApplet(MplPanelClient *panel_client, QWidget *parent) :
    QWidget(parent),
    shutdown_button(this),
    restart_button(this),
    panel(panel_client)
{
    setObjectName(QString::fromUtf8("shutdown_applet"));

    shutdown_button.setObjectName(QString::fromUtf8("shutdown_button"));
    shutdown_button.setFocusPolicy(Qt::NoFocus);
    shutdown_button.setText(QString::fromUtf8(_("Shutdown")));
    vlayout.addWidget(&shutdown_button);

    int runlevel;
    char c;
    FILE *file = popen("runlevel", "r");
    if (file) {
        if (fscanf(file, "%c %d", &c, &runlevel) != 2)
            runlevel = 0;
    }

    if (runlevel == 4) {
        shutdown_eject_button.setObjectName(QString::fromUtf8("shutdown_eject_button"));
        shutdown_eject_button.setFocusPolicy(Qt::NoFocus);
        shutdown_eject_button.setText(QString::fromUtf8(_("Shutdown and eject the key")));
        vlayout.addWidget(&shutdown_eject_button);

        QObject::connect(&shutdown_eject_button, SIGNAL(clicked(bool)), this, SLOT(shutdown_eject_cb()));
    }

    restart_button.setObjectName(QString::fromUtf8("restart_button"));
    restart_button.setFocusPolicy(Qt::NoFocus);
    restart_button.setText(QString::fromUtf8(_("Reboot")));
    vlayout.addWidget(&restart_button);

    setWindowTitle(QString::fromUtf8(_("Shutdown Applet")));

    QObject::connect(&shutdown_button, SIGNAL(clicked(bool)), this, SLOT(shutdown_cb()));
    QObject::connect(&restart_button,  SIGNAL(clicked(bool)), this, SLOT(restart_cb()) );

    vlayout.setAlignment (Qt::AlignCenter);
    setLayout(&vlayout);
}

int
BloomShutdownApplet::shutdown_cb ()
{
    shutdown_button.setText(QString::fromUtf8(_("Shutdown in progress")));
    return system("halt");
}

int
BloomShutdownApplet::shutdown_eject_cb ()
{
    shutdown_button.setText(QString::fromUtf8(_("Shutdown in progress")));
    int ret = system("vbox-set-property /UFO/EjectAtExit 1");
    if (ret) return ret;
    return system("halt");
}

int
BloomShutdownApplet::restart_cb ()
{
    restart_button.setText(QString::fromUtf8(_("Reboot in progress")));
    return system("reboot");
}

void BloomShutdownApplet::changeEvent(QEvent* event)
{
    if(event->type() == QEvent::ActivationChange) {
        if (!isActiveWindow())
            mpl_panel_client_request_hide(panel);
    }
}
