//      BloomShutdownApplet.h
//
//      Copyright Agorabox 2010 <antonin.fouques@agorabox.org>
//
//      This program is free software; you can redistribute it and/or modify
//      it under the terms of the GNU General Public License as published by
//      the Free Software Foundation; either version 2 of the License, or
//      (at your option) any later version.
//
//      This program is distributed in the hope that it will be useful,
//      but WITHOUT ANY WARRANTY; without even the implied warranty of
//      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//      GNU General Public License for more details.
//
//      You should have received a copy of the GNU General Public License
//      along with this program; if not, write to the Free Software
//      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
//      MA 02110-1301, USA.

#ifndef HEADER_SHUTDOWN_APPLET
#define HEADER_SHUTDOWN_APPLET
#define __GTK_BINDINGS_H__

#include <QtGui/QWidget>
#include <glib/gi18n.h>
#include <moblin-panel/mpl-panel-common.h>
#include <moblin-panel/mpl-panel-qt.h>

class BloomShutdownApplet : public QWidget
{
    Q_OBJECT

public:
    BloomShutdownApplet (MplPanelClient *panel_client, QWidget *parent = 0);

private slots:
    int shutdown_cb ();
    int shutdown_eject_cb ();
    int restart_cb ();

private:
    void changeEvent(QEvent* event);

    QVBoxLayout vlayout;
    QPushButton shutdown_button;
    QPushButton shutdown_eject_button;
    QPushButton restart_button;
    MplPanelClient *panel;
};

#endif
